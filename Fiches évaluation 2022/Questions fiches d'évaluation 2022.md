# Questions sur les fiches d'évaluation 2022

Questions globales :

* Existe-t-il un tableau de correspondance entre les fiches 2014 et celles 2022 ? Et ce dans les deux sens (certaines sont nouvelles, certaines ont disparu)
* Quel est "l'esprit" de ces nouvelles fiches d'évaluation ? Quels défauts corrigent-elles ? Qu'apportent-elle de plus ?

Questions spécifiques :

* À quelle entrée rattacher le QuickStop ? 2S5 ? Mais quid des conditions plus fortes ?
* Le spi n'apparait plus en tant que tel ?
* À quel item rattacher la mise à la cape ? 2S5 ?
* À quel item rattacher la limite de faseyement ? 2C1 ?
