# Fiche numéro : 3M2.3 SPI empannage

## Compétence visée

Réaliser les manoeuvres relatives au spinnaker

## Projet de nav

- [x] Sensation
- [x] performance
- [x] exploration

## Conditions météo idéales

- [x] pétole
- [x] petit temps
- [x] medium


## Objectif

Empanner sous spi.

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

TODO

## *Brief*

TODO

## Matériel nécessaire

TODO

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice

TODO

## Mise en place

* remonter au vent du plan d'eau & préparer l'envoi
* virer bord et envoyer
* dans la foulée, empanner
* affaler, remonter au vent
* tourner aux postes et recommancer

TODO

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussi l'exercice ; réponse exclusive : oui ou non

TODO

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

TODO

## Variante -

TODO

## *Debrief* et apports théoriques

TODO

