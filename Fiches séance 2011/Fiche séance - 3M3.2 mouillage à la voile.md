# Fiche numéro : 3M3.2 mouillage à la voile

## Compétence visée

Appareiller ou quitter un mouillage ou un coffre à la voile

## Projet de nav

- [ ] Sensation
- [ ] performance
- [x] exploration

## Conditions météo idéales

- [x] pétole
- [x] petit temps
- [ ] medium
- [ ] gros temps

## Objectif

Savoir mouiller (et repartir) sans moteur

## Matériel nécessaire

* ancre :)

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

TODO

## *Brief*

TODO

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice

TODO

## Mise en place

* la barreur est chef de manoeuvre
* arriver sur zone de mouillage, à vitesse 0, sous GV seule
* mouiller et laisser le bateau faire le drapeau
* repartir 

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

* le bateau ne dérape pas, et bien tenu par son ancre
* au départ du mouillage, l'ancre n'est pas trainée au sol 
* au départ du mouillage, l'ancre ne tape pas contre la coque 

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

TODO

## Variante -

* agrandir la zone de mouillage

## *Debrief* et apports théoriques

* faire dessiner la chaine au sol et le bateau au mouillage
* quelles sont les forces en présence ?

