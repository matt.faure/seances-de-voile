# Fiche numéro : 3M2.3 Spi envoi affalage

## Compétence visée

Réaliser les manœuvres relatives au spinnaker

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] petit temps

## Objectif

> réponse à la question "pourquoi je suis sur l'eau", ou alors "être capable de ..." (virer de bord en maintenant l'équilibre du support, naviguer au près, etc)

Savoir envoyer et affaler le spi

## Pré-requis

> compétence à maîtriser pour pouvoir suivre la séance

## *Brief*


## Matériel nécessaire


## Consignes

consigne sécu : prévoir de l'eau à courir pour l'affalage

## Mise en place

* remonter au vent du plan d'eau & préparer l'envoi
* virer bord et envoyer
* affaler "dans la foulée"
* tourner aux postes et recommencer

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

À l'envoi :

* le spi est en tête, sans cocotte

À l'affalée : 

* le spi est rentré, sans passer dans l'eau

## Observable

> comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

* envoyer le spi au passage de bouée
* affaler le spi pour être au près à la bouée sous le vent, et passer à moins de 2 longueurs de la bouée

## Variante -

## *Debrief* et apports théoriques

* Quelle gîte de bateau pour l'envoi ?
* besoin de coordination dans l'équipage