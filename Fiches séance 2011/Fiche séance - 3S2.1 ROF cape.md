# Fiche numéro : 3S2.1 ROF cape

## Compétence visée

Maitriser les manœuvres courantes de sécurité (homme à la mer, départs et
arrivées dans des conditions difficiles, arrêt rapide)

## Projet de nav

- [ ] Sensation
- [ ] performance
- [ ] exploration

## Conditions météo idéales

- [x] pétole
- [x] petit temps
- [x] medium
- [x] gros temps

## Objectif

Savoir récupérer un objet flottant à la ralingue

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

TODO

## *Brief*

TODO

## Matériel nécessaire

TODO

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice

TODO

## Mise en place

> = déroulement de la séance

TODO

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussi l'exercice ; réponse exclusive : oui ou non

TODO

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

TODO

## Variante -

TODO

## *Debrief* et apports théoriques

TODO

