# Fiche numéro : 3C1.3 se représenter trajectoires et allures

## Compétence visée

Se représenter les trajectoires à suivre et les allures à prendre pour rejoindre 
un point donné du plan d’eau

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] petit temps
- [x] medium
- [x] gros temps

## Objectif

Savoir aller d'un point A à un point B, tout en nommant les allures empruntées

## Matériel nécessaire

* une bouée

## Pré-requis /  *Brief*

* Faire dessiner la rose des allures
* Avoir montré un schéma du PCQ avec les amers remarquables

## Consignes

> - de mise en place
> - de sécurité générale
> - de sécurité spécifiques à l'exercice

## Mise en place

* le barreur est chef de manœuvre
* demander de passer la bouée :
  * à une allure donnée,
  * à une amure donnée
* à chaque changement d'allure, nommer l'allure
* une fois la bouée franchie, tourner aux postes et recommencer

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> comment le stagiaire se rend compte qu'il réussi l'exercice ; réponse exclusive : oui ou non

* après un changement d'allure, la nouvelle allure est nommée et correcte
* la bouée est franchie à la bonne allure / amure

## Observable

> comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

TODO

## Variante -

* Choisir une trajectoire ne nécessitant pas de changement d'amure (juste abattre ou lofer)
* Utiliser l'axe du canal pour se positionner (au milieu du PCQ)

## *Debrief* et apports théoriques

* rose des allures
* cône de déventement
* lofer / abattre != changement d'amure
