# 3C1.1

## Compétence visée

Maintenir l’équilibre du support en utilisant la voile (au près)

## Projet de nav

- [x] performance

## Objectif

Choquer la GV en cas de survente

## Pré-requis

* Savoir naviguer à l'allure de près

## Matériel nécessaire


## Conditions météo idéales

- [x] medium
- [x] gros temps

Tramontane ou autre vent rafaleux

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice


## Mise en place

Marseillan, par tram:

* traverser le plan d'eau au près, passer sous le vent de Marseillan 
  (couloir de la mort) pour toucher les rafales

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> comment le stagiaire se rend compte qu'il réussi l'exercice ; réponse exclusive : oui ou non

* la gîte varie peu (/!\ pas assez objectif ni assez mesurable !)
* le bateau fait cap constant dans les rafales (et sans intervention du barreur)

## Observable

> comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

* reborder la GV suffisamment rapidement pour ne pas trop perdre de vitesse
* gérer la survente avec le chariot de GV (nécessite un bateau sur lequel c'est faisable, i.e. pas Surprise)

## Variante -

## Points à aborder au *debrief*

* Lequel de nos 5 sens est le plus utilisé ? (le touché / l'équilibre)