# Fiche numéro : 3M1.3 Empennage seau d'eau

## Compétence visée

Changer d’amure en contrôlant l’équilibre du navire

## Projet de nav

- [x] Sensation
- [x] performance

## Conditions météo idéales

- [x] pétole
- [x] petit temps
- [x] medium
- [x] gros temps


## Objectif

Empanner en contrôler l'équilibre du bateau

## Pré-requis

> compétence à maîtriser pour pouvoir suivre la séance

TODO

## *Brief*

TODO

## Matériel nécessaire

TODO

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice

TODO

## Mise en place

* remplir le seau d'eau à ras bord
* empanner
* mesurer la quantité d'eau perdue
* faire tourner à chaque poste

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

* le seau d'eau ne perd pas une goutte d'eau !

## Observable

> comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

TODO

## Variante -

TODO

## *Debrief* et apports théoriques

* être vigilant aux déplacements des équipiers au portant
* bouger la barre avec délicatesse, progressivement et sans à-coup

