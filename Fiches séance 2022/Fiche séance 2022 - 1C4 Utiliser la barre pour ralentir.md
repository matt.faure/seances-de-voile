# Fiche numéro : 1C4 Utiliser la barre pour ralentir

## Compétence visée

Utiliser la barre pour ralentir

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Comprendre qu'il est possible de ralentir sans toucher les voiles, mais en utilisant la barre.

## Matériel nécessaire

## Pré-requis

- 1E3.2 Deux familles d'allure
- Savoir estimer la vitesse du bateau sans speedo (en regardant les écoulements par exemple)

## *Brief*

## Consignes de sécurité

Pour le moniteur :

- ne pas dépasser le grand largue afin d'éviter l'empannage.

## Mise en place

- Se placer au bon plein, avec de l'eau à courir
- Laisser les voiles telles quelles.
- Demander à la barreuse quoi faire pour ralentir le bateau. Expérimenter.
- Changer d'amure (sinon la suivante n'a qu'à "répéter") et tourner aux postes.

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Est-ce que le bateau a ralenti ?

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

- Partir d'une allure de largue.

## Variante -

- Partir d'une allure de près.

## *Debrief* et apports théoriques

- Il existe une zone où le bateau d'avance plus. Wouaw !
- Dessiner le cône de déventement.
