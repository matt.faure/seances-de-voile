# Fiche numéro : 3M1.2 Ciseaux

## Compétence visée

Maintenir l’équilibre du support en utilisant la barre (au portant)

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] petit temps

## Objectif

Tenir le vent arrière sans empanner, i.e. travailler la finesse de barre au vent arrière

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Matériel nécessaire

## Consignes

> - Consignes de mise en place
> - Consignes de sécurité générale
> - Consignes sécurité spécifiques à l'exercice

## Mise en place

* au vent du plan d'eau, abattre jusqu'au vent arrière sans empanner
* quand le point d'écoute de la voile d'avant tombe, passer la voile d'avant de l'autre côté
* maintenir le cap vent arrière sans empanner

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

* Conserver l'amure (ne pas empanner)
* Le bateau reste au vent arrière
* Le bateau est en ciseaux sans empanner pendant 1 minute

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

* jouer sur les poids et remonter fausse panne

## Variante -

* rester vent arrière (SANS ciseaux) sans empanner pendant 2 minutes

## *Debrief* et apports théoriques

* quels sont vos (nouveaux ?) repères pour grand largue et vent arrière ?


