# Fiche numéro : 2C1.1 Limite de faseyement

## Compétence visée

2C1 Adapter l'ouverture de la voile à l'allure

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] medium

## Objectif

Savoir régler une voile aux allures de finesse

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

Si ma voile faseye, je me doute qu'elle est mal réglée. Mais si je la borde trop, quand est-elle bien réglée ?

## Matériel nécessaire

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- Se placer bon plein et conserver l'allure
- l'équipière fait faseyer la voile d'avant
- l'équipière règle jusqu'à trouver la limite de faseyement
- Changer d'amure (sinon trop facile pour la suivante) et tourner aux postes

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- Si on choque 2 cm, la voile d'avant faseye.

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques