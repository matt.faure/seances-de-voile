# Fiche numéro : 1M1.1 Virement de bord

## Compétence visée

1M1.1 Utiliser la barre pour changer d'amure

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

> Réponse à la question "pourquoi je suis sur l'eau", ou alors "être capable de ..." (virer de bord en maintenant l'équilibre du support, naviguer au près, etc)

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- le bateau a changé d'amure, en faisant passer le vent par *l'avant* du bateau
- le bateau est sur une allure de finesse

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

