# Fiche numéro : 3N2.1 Passage de bouée

## Compétence visée

Se représenter les trajectoires à suivre et les allures à prendre pour rejoindre un point donné du plan

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] medium

## Objectif

- Construire la trajectoire du bateau pour rallier un point donné sur le plan d'eau (virements de bord, empannages,
aulofées, abattées)
- Passer la bouée à une allure de bon-plein (peu importe l'amure)

## Pré-requis

- lofer / abattre
- virer / empanner

## *Brief*

- Dessiner le vent, la bouée, le bateau
- Expliquer l'objectif
- Dessiner des trajectoires possibles

## Matériel nécessaire

- 1 bouée frite

## Consignes

/!\ rester proche de la bouée (moins de 5 longueurs)

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Barreuse (cheffe de manœuvre) : 

- le bateau passe la bouée au bon-plein
- la bouée est à moins de 2 mètres du bord du bateau

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

## Variante -

Barreuse (cheffe de manœuvre) :

- le bateau passe la bouée au bon-plein
- la bouée est à moins d'une longueur du bord du bateau

## *Debrief* et apports théoriques

- Demander aux stagiaires de représenter sur le tableau la trajectoire du bateau depuis un point X vers la bouée
- Demander de nommer chaque changement de direction (aulofée, abattée, VdB, empannage)

