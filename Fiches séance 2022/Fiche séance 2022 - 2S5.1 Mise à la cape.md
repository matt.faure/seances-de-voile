# Fiche numéro : 2S5.1 Mise à la cape

## Compétence visée

Arrêter le navire en cas d’incident ou d’accident

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] medium

## Objectif

Savoir se mettre à la cape

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Matériel nécessaire

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

> = déroulement de la séance

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

* le bateau ne vire pas ou n'empanne pas

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

- Dessiner la trajectoire du bateau à la cape (sur les deux amures)