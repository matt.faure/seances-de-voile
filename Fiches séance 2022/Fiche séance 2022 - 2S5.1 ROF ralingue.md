# Fiche numéro : 2S5.1 ROF ralingue

## Compétence visée

2S5 Expérimenter les manœuvres courantes de sécurité dans des conditions aménagées

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Savoir récupérer un objet flottant à la ralingue

## Pré-requis

- 3N2.1 Passage de bouée

## *Brief*

- Arriver sur la bouée au bon-plein,
- À vitesse 0
- La bouée au niveau des haubans

## Matériel nécessaire

TODO

## Consignes

- Ne pas se prendre la quille dans la bouée (attention à la gîte)

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- la bouée reste : à moins d'un mètre du bateau ET pendant au moins 10 secondes

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

- refaire l'exercice sur l'autre amure

## Variante -

- la bouée reste : à moins d'un mètre du bateau
- OU ALORS le bateau est immobile (mais plus éloigné de la bouée)

## *Debrief* et apports théoriques

