# Fiche numéro : 2C1.2 Lofer border Abattre choquer

## Compétence visée

Adapter l’ouverture de la voile à l’allure

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Avoir en permanence des voiles réglées, quelle que soit l'allure.

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- Une équipière choisit une allure
- La barreuse s'y met
- Les équipières règlent les voilent

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- Au près, les voiles sont réglées à la limite de faseyement
- Au portant, les voiles ne faseyent pas (à améliorer)

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

