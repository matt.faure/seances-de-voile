# Fiche numéro : 1C1.1 Gonfler la voile pour avancer au portant

## Compétence visée

- 1C1 Gonfler la voile pour avancer
- 1C2 Utiliser la voile pour ralentir

(Note perso : je n'ai pas réussi à faire de séance distincte pour 1C2)

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] medium

## Objectif

Comprendre ce qui permet au bateau d'avancer quand il se trouve au près

## Matériel nécessaire

## Pré-requis

- 1E3.2 Deux familles d'allure
- Savoir estimer la vitesse du bateau sans speedo (en regardant les écoulements par exemple)

## *Brief*

- Comprendre un des fondamentaux : voile = propulsion
- Cela peut paraître bête, mais ce n'est pas forcément évident, alors nous allons prendre quelques minutes pour le
  décrire

## Consigne de sécurité

Pour le moniteur :

- être vigilant à bien conserver l'allure (prendre la barre ?)
- être vigilant à ne pas empanner
- être vigilant à la houle

Pour les stagiaires : c'est une expérimentation pédagogique, il se peut que ça fasse flop !

## Consignes de mise en place

* Se placer au largue, avec de l'eau à courir.
* Demander à la barreuse de viser un point dans le paysage (correspondant à l'allure souhaitée).
* Préciser à la barreuse de tenir fermement le cap, peut-être le bateau ne sera pas d'accord ! Auquel cas, le dire sera
  utile à tout l'équipage.
* Laisser la voile d'avant telle quelle.

## Mise en place

* Faire observer la vitesse du bateau avec les écoulements à l'arrière du bateau
* Demander aux stagiaires quoi faire pour faire avancer le bateau. Expérimenter.
* Quelles observations quant à la vitesse du bateau ?
* Est-il possible de refaire les mêmes actions qu'au près ?
* Virer de bord (sinon la suivante n'a qu'à "répéter") et tourner aux postes. Note : préférer un virement à un empannage.

/!\ Être à l'écoute de toutes les suggestions des stagiaires et les expérimenter.

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Le bateau a-t-il accéléré ?

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

- Au portant, choquer fait accélérer
- Border fait (un peu) ralentir, mais il faut tenir la barre fermement.
- Aux allures de portant, le vent *pousse* dans les voiles (notion de poussée)
- Parler des Égyptiens et des Romains qui ne connaissaient que le portant (!)

