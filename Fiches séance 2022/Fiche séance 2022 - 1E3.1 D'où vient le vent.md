# Fiche numéro : 1E3.1 D'où vient le vent

## Compétence visée

Au port ou à l'arrêt, sur l'eau, savoir d'où vient le vent

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Trouver l'origine du vent, sans aucune aide extérieure

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussit l'exercice ; réponse exclusive : oui ou non

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

