# Fiche numéro : 2M1.2 Empanner sans s'arrêter

## Compétence visée

Virer de bord et empanner sans s'arrêter

## Projet de nav

- [ ] Sensation
- [ ] performance
- [ ] exploration

## Conditions météo idéales

- [ ] pétole
- [x] petit temps
- [x] medium
- [x] gros temps

## Objectif

Empanner sans arrêter le bateau

## Matériel nécessaire

TODO

## Pré-requis

* 1E3 Au port ou à l'arrêt, sur l'eau, savoir d'où vient le vent
* 1M1 Utiliser la barre pour changer d'amure
* 1M2 Se déplacer pour changer de côté sans compromettre la manœuvre

## *Brief*

TODO

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

TODO

## Mise en place

* la personne évaluée est le barreur
* se placer au grand largue
* le barreur est chef de manœuvre et lance l'empannage
* Il annonce la fin de l'empannage

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

* le bateau est toujours en mouvement pendant la manoeuvre (ne s'arrête pas)
* la bateau est sur une allure de portant

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

* Sortir de l'empannage au grand largue
* Bateau à plat tout au long de l'empannage

## Variante -

* Sortie d'empannage : à une allure de portant

## *Debrief* et apports théoriques

* Dessinez la rose des allures pour le portant
* Quid de l'assiette du bateau ?
  * l'équilibre du bateau est important => conséquence sur le déplacement des équipiers
* Quid de la barre en sortie d'empannage ?
  * Nécessité de bien tenir la barre en sortie d'empannage

