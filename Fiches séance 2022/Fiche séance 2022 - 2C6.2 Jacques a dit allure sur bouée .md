# Fiche numéro : 2C6.2 Jacques a dit allure sur bouée

## Compétence visée

2C6 Identifier son allure de navigation

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

- Arriver à prendre une allure, tout en visant un point donné sur le plan d'eau.

## Matériel nécessaire

- une bouée frite (seau et parre-battage sont OK mais moins visibles)

## Pré-requis

## *Brief*

## Consignes de mise en place

Pour la monitrice :

- rester proche de la bouée (<10 longueurs)

## Mise en place

- Une équipière choisit une allure
- la barreuse vise la bouée à l'allure donnée

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- l'allure demandée est tenue
- la bouée est passée à moins d'une longueur de bateau

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

## Variante -

- Oublier la bouée et seulement tenir une allure

## *Debrief* et apports théoriques

- Au tableau, dessiner l'infinité de chacune des allures
- Mettre en évidence les deux seules permettant d'atteindre la bouée 
