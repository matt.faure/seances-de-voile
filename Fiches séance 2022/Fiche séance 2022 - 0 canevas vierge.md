# Fiche numéro : TODO

## Compétence visée

> cf item sur la *carte de progression FFV* ou la *fiche d'évaluation GLÉNANS*

## Projet de nav

- [ ] Sensation
- [ ] performance
- [ ] exploration

## Conditions météo **idéales**

- [ ] pétole
- [ ] petit temps
- [ ] medium
- [ ] gros temps

## Objectif

> Réponse à la question "pourquoi je suis sur l'eau", ou alors "être capable de ..." (virer de bord en maintenant l'équilibre du support, naviguer au près, etc)

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussit l'exercice ; réponse exclusive : oui ou non

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

