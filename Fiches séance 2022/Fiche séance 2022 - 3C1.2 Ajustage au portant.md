# Fiche numéro : 3C1.1 Ajustage au portant

## Compétence visée

Régler le volume des voiles en statique (hâle-bas, cunningham, bordure, lattes)

## Projet de nav

- [x] performance

## Conditions météo idéales

- [ ] pétole
- [ ] petit temps
- [x] medium
- [ ] gros temps

## Objectif

Savoir affiner le réglage des voiles au près

## Matériel nécessaire

## Pré-requis

- 2C1.1 Limite de faseyement

## *Brief*

* Vocabulaire :
    * réglage = recherche limite de faseyement
    * ajustage = recherche de performance au-delà de cette limite
* GV : ouvrir la bôme le plus possible (chariot de GV)
* GV : tirer la bôme vers le bas (hâle-bas) pour plus de puissance
* GV : hâle-bas = fusible si spi compliqué
* Voile d'avant : envoyer le spi :D !

## Consignes

## Mise en place

* Sur un long bord de portant, travailler l'ajustage et mesurer la modification de vitesse au speedo (ou à la montre)

## Repères

## Critères de réussite

## Observable

## Variante +

- par medium+, comment être moins sujet aux écarts de barre ? (en choquant du hâle-bas)

## Variante -

## *Debrief* et apports théoriques

Aéro :

- haut de la GV = petite surface mais très puissante (d'où les GV à corne)
- expliquer comment
