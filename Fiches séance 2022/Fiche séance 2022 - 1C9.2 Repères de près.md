# Fiche numéro : 1C9.2 Repères de près

## Compétence visée

> cf item sur la *carte de progression FFV* ou la *fiche d'évaluation GLÉNANS*

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Identifier par son corps les 3 repères de près

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

Aux allures de près, il existe des repères physiques manifestes. Les connaître nous permettra de nous assurer que nous
sommes bien au près.

Prendre conscience que le corps tout entier permet d'obtenir des informations sur le bateau.

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- Se placer au largue
- Demander aux stagiaires quelles *sensations* ils ressentent (en utilisant leurs 5 sens)
- Se placer au près et se poser la même question.
- Si besoin, retourner sur l'autre famille d'allure et comparer. 

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Avoir identifié : 

- la gite
- la sensation de vitesse (vent sur le visage)
- le bruit des écoulements

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

- Qu'est-ce qui procure la sensation de vitesse au près (alors qu'on ne l'a pas au portant) ?
- Introduire les notions de vent réel et vent apparent.
- pour la gite, en profiter pour introduire un peu d'aero/hydro en décrivant les forces en présence
