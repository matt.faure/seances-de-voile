# Fiche numéro : 1C1.1 Gonfler la voile pour avancer au près

## Compétence visée

- 1C1 Gonfler la voile pour avancer
- 1C2 Utiliser la voile pour ralentir

(Note perso : je n'ai pas réussi à faire de séance distincte pour 1C2)

## Projet de nav

- [x] exploration

## Conditions météo idéales

- [x] medium

## Objectif

Comprendre ce qui permet au bateau d'avancer quand il se trouve au près

## Matériel nécessaire

## Pré-requis

- 1E3.2 Deux familles d'allure
- Savoir estimer la vitesse du bateau sans speedo (en regardant les écoulements par exemple)

## *Brief*

- Comprendre un des fondamentaux : voile = propulsion
- Cela peut paraître bête, mais ce n'est pas forcément évident, alors nous allons prendre quelques minutes pour le
  décrire

## Consignes de mise en place

* Se placer au près ou au bon plein, avec de l'eau à courir
* Demander à la barreuse de viser un point dans le paysage (correspondant à l'allure souhaitée)
* Laisser la voile d'avant telle quelle
* Faire faseyer la GV

## Mise en place

* Faire observer la vitesse du bateau avec les écoulements à l'arrière du bateau
* Demander aux stagiaires quoi faire pour faire avancer le bateau. Expérimenter.
* Quelles observations quant à la vitesse du bateau ?
* Virer de bord (sinon la suivante n'a qu'à "répéter") et tourner aux postes.

/!\ Être à l'écoute de toutes les suggestions des stagiaires et les expérimenter.

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Le bateau a-t-il accéléré ?

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

- Aux allures de finesse, le vent *glisse* dans les voiles (notion d'écoulement)
- Parler du principe de l'aile d'avion.
- Faire la démonstration en soufflant sur une face d'une feuille de papier (la feuille se lève).
- Autre démo petite cuillère avec eau
- Faire le lien avec la séance "gonfler ma voile pour avancer au portant".

