# Fiche numéro : 3C1.1 Ajustage au près

## Compétence visée

Régler le volume des voiles en statique (halebas, cunningham, bordure, lattes)

## Projet de nav

- [x] performance

## Conditions météo idéales

- [ ] pétole
- [ ] petit temps
- [x] medium
- [ ] gros temps

## Objectif

Savoir affiner le réglage des voiles au près

## Matériel nécessaire

## Pré-requis

- 2C1.1 Limite de faseyement

## *Brief*

* Vocabulaire :
    * réglage = recherche limite de faseyement
    * ajustage = recherche de performance au-delà de cette limite
* Voile d'avant : penons doivent être parallèles (tension écoute ou barre)
* Voile d'avant : penons doivent être parallèles, sur tous les étages (chariot VA)
* Voile d'avant : choquer un peu de drisse si petit temps
* GV : bôme sur ligne de foi du bateau (écoute + chariot GV), mais pas plus
* GV : penons avec écoulement laminaire sur tous les étages (écoute + chariot GV)
* GV : petit temps : choquer de la drisse, et/ou du cunningham, et/ou bosse d'empointure
* GV : gros temps : reprendre de la drisse, et/ou du cunningham, et/ou bosse d'empointure
* Note : prendre du pataras permet juste de retarder la prise de ris

## Consignes

## Mise en place

* Sur un long bord de près, travailler l'ajustage et mesurer la modification de vitesse au speedo (ou à la montre)

## Repères

## Critères de réussite

## Observable

## Variante +

- quelle limites à cet exercice dans une mer formée ?
- Réponse : il faut conserver les voiles puissantes pour passer les vagues, donc un peu plus creuses.

## Variante -

- Variante 1 : se concentrer sur un étage de penons et les placer à l'horizontale
- Variante 2 : ensuite travailler sur le chariot de VA afin que les 3 étages décrochent ensemble.

## *Debrief* et apports théoriques

Aéro :

- voile creuse => plus de puissance
- voile plate => plus de cap

Hydro :

- mer agitée => creuser un peu pour avoir plus de puissance et passer vagues / clapot

