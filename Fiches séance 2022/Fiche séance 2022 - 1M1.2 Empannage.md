# Fiche numéro : 1M1.2 Empannage

## Compétence visée

1M1.1 Utiliser la barre pour changer d'amure

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

> Réponse à la question "pourquoi je suis sur l'eau", ou alors "être capable de ..." (virer de bord en maintenant l'équilibre du support, naviguer au près, etc)

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

## Consignes

- sécu : ne pas être sur la trajectoire de la bôme ni du palan

## Mise en place

> = déroulement de la séance

## Repères

- le bateau a changé d'amure, en faisant passer le vent par *l'arrière* du bateau
- le bateau est sur une allure de portant

## Critères de réussite

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

- faire rédiger la chrono