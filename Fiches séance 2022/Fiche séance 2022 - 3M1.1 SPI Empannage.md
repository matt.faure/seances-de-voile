# Fiche numéro : 3M1.1 SPI Empannage

## Compétence visée

3M1 Connaitre et effectuer les tâches de chaque poste lors des manœuvres

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] petit temps

## Objectif

Savoir empanner sous spi.

## Matériel nécessaire

## Pré-requis

- savoir barrer vent arrière sans empanner (la séance "voiles en ciseaux" est utile)

## *Brief*

Démystifions l'empannage sous spi. Il y a un peu plus de pré-requis (que nous allons aborder) et d'exigence
synchronisation, mais la manœuvre reste un empannage, tout simple !

## Consignes

Consignes de sécurité :

- La n°1 est toujours **au vent** du tangon
- La n°1 n'est **jamais** entre le tangon et le mat
- Ne pas se trouver sur le trajet de la bôme ou du palan de GV

Consignes de mise en place :

- La barreuse ne doit s'intéresser qu'à une seule chose : conserver le spi gonflé. Si le spi est déventé, il risque de
  s'enrouler autour de l'étais ou sur lui-même, et là c'est le vrac.
- Le dialogue barreuse / régleuses (pluriel) est important
- Le tangon doit être horizontal

## Mise en place

- lancer la chronologie
- au vent-arrière, le tangon doit être perpendiculaire au vent (donc au bateau). Il ne devrait pas y avoir d'effort sur
  la cloche. Si c'est pourtant le cas, c'est qu'un truc ne va pas (sommes-nous bien vent arrière ?)
- la GV est empannée en même temps que le spi (voire après, mais pas avant, sinon on risque de déventer le spi)

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- le bateau a changé d'amure
- le bateau est sur une allure de portant
- le bateau est resté à plat tout au long de la manœuvre

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

- Introduire la gestion de la contre-écoute de foc dans la chronologie

## Variante -

- Avoir préalablement bordé la GV dans l'axe. Cela fait une tâche de moins, mais c'est plus exigeant pour la barreuse.
- Empanner sans spi (oui oui) : toutes les ficelles sont présentes, accrochées en araignée. Cela permet de voir plus
  facilement le travail à faire pour la n°1.

## *Debrief* et apports théoriques

- Faire rappeler la chronologie
- Quel est **le** point qui vous surprend ? vous interpelle ?
- Faire un schéma aéro/hydro simple pour illustrer un déséquilibre provoquant un départ au lof
    - faire le lien avec l'importance d'avoir un bateau à plat
    - Question : comment ça se passe avec de la houle ?
        - réponse rapide : c'est beaucoup plus exigeant pour la barreuse de conserver le bateau à plat
        - réponse rapide 2 : ça peu finir en départ au log ou à l'abattée, cf les vidéos.
