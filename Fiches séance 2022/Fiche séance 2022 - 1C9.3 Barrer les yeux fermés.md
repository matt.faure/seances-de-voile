# Fiche numéro : 1C9.3 Barrer les yeux fermés

## Compétence visée

> cf item sur la *carte de progression FFV* ou la *fiche d'évaluation GLÉNANS*

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Mettre en pratique les repères de près

## Matériel nécessaire

## Pré-requis

- 1M9.1 Repères de près

## *Brief*

## Consignes de sécurité

- Pour les stagiaires : à la demande se baisser et baisser les têtes.
- Pour la monitrice : être vigilant à l'empannage. 

## Mise en place

- Se placer bon plein
- L'équipage ne touche pas au réglage des voiles
- Bander les yeux de la barreuse
- Rester dans les allures de finesse pendant 2 minutes

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Pendant les deux minutes : 

- l'amure est toujours la même
- le bateau est sur les allures de finesse.

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

- sur la même allure de finesse (soit près, soit bon plein, mais pas les deux !)

## Variante -

## *Debrief* et apports théoriques

Vos ressentis ?