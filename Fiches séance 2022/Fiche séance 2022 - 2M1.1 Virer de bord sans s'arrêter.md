# Fiche numéro : 2M1.1 Virer de bord sans s'arrêter

## Compétence visée

Virer de bord et empanner sans s'arrêter

## Projet de nav

- [ ] Sensation
- [ ] performance
- [ ] exploration

## Conditions météo idéales

- [ ] pétole
- [x] petit temps
- [x] medium
- [x] gros temps

## Objectif

Virer de bord sans arrêter le bateau

## Matériel nécessaire

TODO

## Pré-requis

* 1E3 Au port ou à l'arrêt, sur l'eau, savoir d'où vient le vent
* 1M1 Utiliser la barre pour changer d'amure
* 1M2 Se déplacer pour changer de côté sans compromettre la manœuvre

## *Brief*

TODO

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

TODO

## Mise en place

* la personne évaluée est le barreur
* se placer au près
* le barreur est chef de manoeuvre et lance le virement
* Il annonce la fin du virement 

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

TODO

## Critères de réussite

* le bateau est toujours en mouvement pendant la manoeuvre (ne s'arrête pas)
* le bateau est sur une allure de finesse

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

TODO

## Variante +

* accélérer ou ralentir le changement d'amure selon les difficultés rencontrées par l'équipage
* Sortie VdB : minimiser perte de vitesse

## Variante -

* réussir à virer (ne pas faire de manque à virer)
* Sortie VdB : à une allure de finesse

## *Debrief* et apports théoriques

* la coordination dans l'équipage est importante. La barreuse doit regarder et le bateau et ses équipières.

