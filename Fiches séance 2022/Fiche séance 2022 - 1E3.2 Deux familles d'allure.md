# Fiche numéro : 1E3.2 Deux familles d'allure

## Compétence visée

Au port ou à l'arrêt, sur l'eau, savoir d'où vient le vent

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Depuis le bateau, savoir identifier à quelle famille d'allure il se trouve.

## Matériel nécessaire

## Pré-requis

- 1E3.1 D'où vient le vent (comment trouver l'origine du vent)

## *Brief*

- Dessiner le bateau et flécher les origines possibles du vent : près ou portant (choisir bon plein et largue).
- Le faire sur les deux amures
- Notes pour le schéma : 
  - Utiliser des couleurs différentes pour le près et le portant
  - ne pas dessiner le cône de déventement
  - ne pas parler du travers
  - représenter les haubans et s'en servir comme point de repère
- /!\ le schéma n'est PAS une rose des allures : 
  - c'est le bateau qu'on représente, et qui ne bouge pas. 
  - ce sont les flèches de vents qui varient autour du même bateau

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- À la demande du moniteur, tout le monde indique d'où vient le vent
- Pour une personne désignée par le moniteur, dire à laquelle des deux familles d'allure le bateau se trouve.
- Commencer sur une même famille d'allure et la même amure. Exemple : bon plein puis près, le tout disons babord amure

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussit l'exercice ; réponse exclusive : oui ou non

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

- Faire l'exercice au portant (même amure d'abord) en changeant d'allure.
- Idem en changeant d'amure
- Introduire le travers (pas évident pour les stagiaires !)
- Alterner toutes les combinaisons

## Variante -

- Faire l'exercice au près, sur une seule amure

## *Debrief* et apports théoriques

- Faire dessiner la rose des allures simplifiée : 2 familles 
- Introduire cône de déventement et travers