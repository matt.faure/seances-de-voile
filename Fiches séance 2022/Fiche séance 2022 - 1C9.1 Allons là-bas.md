# Fiche numéro : 1C9.1 Allons là-bas

## Compétence visée

- 1C1 Gonfler la voile pour avancer
- 1C2 Utiliser la voile pour ralentir
- 1C3 Utiliser la barre pour se diriger
- 1C4 Utiliser la barre pour ralentir

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Gérer barre et voiles, de manière coordonnée

## Matériel nécessaire

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

Exercice de synthèse sur la conduite du bateau : se rendre à un point donné.

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- Le moniteur défini avec ses deux bras une zone allant du près au largue (sur une seule amure)
- La barreuse choisit un point à rallier dans la zone.
- L'équipage amène le bateau à ce point.
- Tourner aux postes et recommencer sur l'autre amure.

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

Le point choisi est atteint.

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

