# Fiche numéro : 2C6.1 Repères de près

## Compétence visée

2C6 Identifier son allure de navigation

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

* Identifier et ressentir les repères caractéristiques des allures de près

## Matériel nécessaire

## Pré-requis

* 1C1 Gonfler la voile pour avancer
* 1C2 Utiliser la voile pour ralentir

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

> = déroulement de la séance

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

> Comment le stagiaire se rend compte qu'il réussit l'exercice ; réponse exclusive : oui ou non

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être : 
	- objectif
	- mesurable
	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

