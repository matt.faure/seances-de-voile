# Fiche numéro : 2C2.1 Conserver une trajectoire fixe au près

## Compétence visée

Conserver une trajectoire fixe au près

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Arriver à viser un point fixe sur le plan d'eau

## Matériel nécessaire

- 1 bouée frite (seau et pare-battage fonctionnent aussi, mais la frite est plus visible)

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

## *Brief*

Jusqu'à présent, nous visions des points vagues dans le paysage. Maintenant, nous souhaitons viser un point précis, fixe
sur le plan d'eau.

## Consignes de mise en place

Pour la monitrice : 

- rester proche de la bouée (<10 longueurs)
- Se placer *sous le vent* de la bouée
- Avoir la bouée **hors** du cône de déventement, i.e. entre le travers et le près

## Mise en place

- Demander à la barreuse d'atteindre la bouée sans virer ni empanner 

## Repères

- La bouée est atteinte (<1 longueur de bateau)
- Aucun empannage ni virement de bord n'a été effectué sur le trajet

## Critères de réussite


## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> - objectif
> - mesurable
> - contrôlable

## Variante +

- absence de "S" dans le sillage (trajectoire rectiligne)
- Ajouter une contrainte sur l'amure, obligeant un virement de bord (N2 car trajectoire indirecte)

## Variante -

## *Debrief* et apports théoriques

