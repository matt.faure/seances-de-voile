# Fiche numéro : 1C9.4 Lofer abattre

## Compétence visée

> cf item sur la *carte de progression FFV* ou la *fiche d'évaluation GLÉNANS*

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] medium

## Objectif

Comprendre qu'on peut lofer sans virer, abattre sans empanner.

## Matériel nécessaire

## Pré-requis

- 1E3.2 Deux familles d'allure

## *Brief*

## Consignes

> - consignes de mise en place
> - consignes de sécurité générale
> - consignes sécurité spécifiques à l'exercice

## Mise en place

- Se placer au près
- Demander à la barreuse de placer le bateau sur une allure de portant (sans pour autant empanner) 
- Changer d'amure (sinon trop facile pour la suivante) et tourner au poste

## Repères

> De quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

- le bateau a changé de famille d'allure
- le bateau a conservé son amure

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

## Variante -

## *Debrief* et apports théoriques

