# Fiche numéro : 4C1.1 SPI Conduite

## Compétence visée

4C1 Adapter les réglages en fonction de l'allure et des conditions de vent et de mer pour faciliter le pilotage.

## Projet de nav

- [x] exploration

## Conditions météo **idéales**

- [x] petit temps

## Objectif

Comprendre les interactions sous spi entre changement d'allure et action sur écoute et bras.

## Matériel nécessaire

## Pré-requis

- Barreuse : bien distinguer largue, grand largue, vent arrière et fausse panne
- Connaitre le vocabulaire du spi (brasser, border)
- Communication :
  - savoir exprimer aux autres équipières ce qu'il se passe pour soi
  - savoir écouter les autres équipières

## *Brief*

Nous allons voir comment maintenir le spi quand on abat ou on lofe

## Consignes

Consigne sécurité :

- on reste sur la même amure (pas d'empannage)

Consignes de mise en place :

* quand on abat, on brasse et on choque ensemble
* quand on lofe, on débrasse et on borde ensemble

## Mise en place

- Sous spi, passer progressivement de largue à vent arrière (sans empanner), et inversement
- la barreuse annonce les abatées et aulofées.

## Repères

## Critères de réussite

- la bordure du spi ne touche pas l'étais
- le spi reste gonflé
- le spi ne claque pas

## Observable

> Comment le moniteur se rend compte que le stagiaire réussi l'exercice. Un observable doit être :
> 	- objectif
> 	- mesurable
> 	- contrôlable

## Variante +

- avec un vent oscillant, conserver une trajectoire rectiligne pour le bateau (plus exigeant pour la barreuse).

## Variante -

## *Debrief* et apports théoriques

* Faire schéma avec spi toujours dans l'axe du vent réel et bateau
  changeant de direction
* faire dessiner bateau et spi : au largue, puis au vent arrière
* Questionner sur quelles ficelles agir, et pourquoi
* Faire dessiner quand on brasse sans choquer
* Faire dessiner quand on débrasse sans border
* Que faire pour conserver une trajectoire rectiligne si le vent refuse
  (réponse : agir sur la barre sans toucher les ficelles)