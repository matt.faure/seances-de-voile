# Fiche numéro : 3-- Prise en main

## Compétence visée

> cf item sur la *carte de progression FFV* ou la *fiche d'évaluation GLÉNANS*

TODO

## Projet de nav

- [ ] Sensation
- [ ] performance
- [ ] exploration

## Conditions météo idéales

- [ ] pétole
- [ ] petit temps
- [ ] medium
- [ ] gros temps


## Objectif

> Réponse à la question "pourquoi je suis sur l'eau", ou alors "être capable de ..." (virer de bord en maintenant l'équilibre du support, naviguer au près, etc)

Reprendre ses marques sur le bateau

## Pré-requis

> Compétence à maîtriser pour pouvoir suivre la séance

TODO

## *Brief*

Se remettre "en mode bateau", reprendre nos marques, nos repères

## Matériel nécessaire

TODO

## Consignes

> - de mise en place
> - de sécurité générale
> - sécurité spécifiques à l'exercice

TODO

## Mise en place

* 2 VdB par personne à chaque poste
* 2 empannages par personne à chaque poste

## Repères

> de quoi les stagiaires ont besoin pour réussir l'exercice

## Critères de réussite

* sortie de VdB : au près
* sortie d'empannage : au grand largue

## Variante +

* Sortie VdB : minimiser perte de vitesse
* Sortie d'empannage : bateau à plat tout le temps

## Variante -

* Sortie VdB : à une allure de finesse
* Sortie d'empannage : à une allure de portant

## *Debrief* et apports théoriques

* Dessinez la rose des allures pour le portant
* qu'est-ce qui vous est revenu ?
